var searchData=
[
  ['pila_5',['pila',['../structpila.html',1,'pila'],['../_pila_8h.html#a7485e6cb2448d1fbed90563c58f45b25',1,'pila():&#160;Pila.h']]],
  ['pila_2eh_6',['Pila.h',['../_pila_8h.html',1,'']]],
  ['pila_5fimpl_2ec_7',['pila_impl.c',['../pila__impl_8c.html',1,'']]],
  ['pila_5fpop_8',['pila_pop',['../_pila_8h.html#af769f408605e6b6f656b42f00ea12dc0',1,'pila_pop(pila *this):&#160;pila_impl.c'],['../pila__impl_8c.html#af769f408605e6b6f656b42f00ea12dc0',1,'pila_pop(pila *this):&#160;pila_impl.c']]],
  ['pila_5fpush_9',['pila_push',['../_pila_8h.html#ac5f9fe002436ed2804e67801949c2ed2',1,'pila_push(pila *this, int val):&#160;pila_impl.c'],['../pila__impl_8c.html#ac5f9fe002436ed2804e67801949c2ed2',1,'pila_push(pila *this, int val):&#160;pila_impl.c']]],
  ['pop_10',['pop',['../structpila.html#af47a6229f1a6e59e7a822b8e80212ad3',1,'pila']]],
  ['push_11',['push',['../structpila.html#a55d29bd5e141a9f0c41f11d0282074fe',1,'pila']]]
];
